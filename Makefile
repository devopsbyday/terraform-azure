###################################
## WORK IN PROGRESS, DO NOT USE!!!!
###################################
SHELL := /bin/bash
$(VERBOSE).SILENT:
.DEFAULT_GOAL := help

# some default variable setting
ACCOUNT_PATH ?= not_set

# derive some other variables
TIER := $(shell echo "${ACCOUNT_PATH}" | cut -d '/' -f2)
ACCOUNT := $(shell echo "${ACCOUNT_PATH}" | cut -d '/' -f3)
ACCOUNT_ENV := $(shell echo "${ACCOUNT_PATH}" | cut -d '/' -f4)

# available options, use regex to string check
RE_ACCOUNT_PATH := ^(org/management/.*|org/shared/.*|org/organization|org/.*/dev|org/.*/test|org/.*/prod)$$
RE_TIER := ^(management|shared|spoke|organization)$$

# build related vars
GIT_HASH := $(shell git rev-parse --short HEAD)

ifneq (,$(findstring spoke,$(ACCOUNT_PATH)))
	TF_PATH := org/_account_template
else ifneq (,$(findstring shared,$(ACCOUNT_PATH)))
	TF_PATH := org/_account_template
else
	TF_PATH := $(ACCOUNT_PATH)
endif

ifeq ($(NEW_VEND), true)
	TF_PATH := org/_new_account_template
endif

export TF_PATH

# terraform related configuration
TF_STATE_KEY := $(ACCOUNT_PATH)/terraform.tfstate
TF_ARTIFACT := $(TIER)-$(ACCOUNT)-$(ACCOUNT_ENV)-terraform.tfstate
TF_ARTIFACT_BUCKET := nbs-management-master-terraform-artifacts
TF_VARS := -var "TF_PATH=$(TF_PATH)" $(shell docker-compose run --entrypoint=bash aws scripts/hierarchy.sh $(ACCOUNT_PATH))

# docker-compose calls
AWS := docker-compose run aws
TF := docker-compose run terraform
PROWLER := docker-compose run prowler
CUSTODIAN := docker-compose run custodian
PYTHON := docker-compose run python

ifneq (,$(findstring master, $(ACCOUNT_PATH)))
	ACCOUNT_ID := 113843251217
else ifneq (,$(findstring organization, $(ACCOUNT_PATH)))
	ACCOUNT_ID := 113843251217
else
	ACCOUNT_ID := "$(shell export TF_PATH=$(TF_PATH) && \
			docker-compose run --entrypoint=bash aws scripts/tokenize.sh >&2 && \
			$(TF) init -backend-config=key=$(TF_STATE_KEY) $(TF_VARS) -reconfigure > /dev/null && \
			$(TF) output account-id | tr -d \t)"
endif

##@ Entry Points
.PHONY: lint
lint: _validate _clean ## Validate syntax on current terraform configuration
	echo "Working with [$(TIER)] [$(ACCOUNT)$(ACCOUNT_ENV)] ... Taking vars from [$(ACCOUNT_PATH)] and TF from [$(TF_PATH)]"
	echo "TF arguments: $(TF_VARS)"
	$(TF) fmt
	$(TF) init -input=false -backend=false -lock=false
	$(TF) validate $(TF_VARS)

.PHONY: build
build: _validate _init ## Generate a terraform plan as an output file
	echo "Planning ${TF_ARTIFACT} from ${TF_ARTIFACT_BUCKET} $(TF_PATH) with vars from ${ACCOUNT_PATH}"
	$(TF) plan -lock-timeout=180m $(TF_VARS) -out $(TF_ARTIFACT)

.PHONY: deploy
deploy: _validate _init ## Deploy infrastructure with an expected terraform plan file
	echo "Downloading plan ${TF_ARTIFACT} from ${TF_ARTIFACT_BUCKET}"
	$(TF) apply -lock-timeout=180m $(TF_ARTIFACT)

.PHONY: shell
shell: _init ## Shell into terraform container for local debugging with bash
	export ACCOUNT_ID="$(ACCOUNT_ID)" && \
	docker-compose run --entrypoint=bash terraform

.PHONY: auth
auth: _validate ## Shell into aws container with an assumed role for the relevant account
	echo "Working with [$(TIER)] [$(ACCOUNT)$(ACCOUNT_ENV)] ... [$(ACCOUNT_PATH)]"
	export ACCOUNT_ID="$(ACCOUNT_ID)" && \
	export ACCOUNT="nbs-management-$(ACCOUNT)-$(ACCOUNT_ENV)" && \
	export MAKE_SHELL="yes" && \
	docker-compose run --entrypoint=bash aws scripts/assume-role.sh

.PHONY: tests
tests: ## Currently runs assume-roles-test.sh
	docker-compose run --entrypoint=bash aws tests/assume-role/assume-roles-test.sh

.PHONY: unlock
unlock: ## Removes state file lock for those troublesome crashed. Usage; make force-unlock LOCK_ID=[lockId]
	${TF} force-unlock -force ${LOCK_ID}

.PHONY: prowler
prowler: _clean ## Execute prowler CIS check against account provided
	$(PROWLER) ./prowler ${ARGS} ACCOUNTS=${ACCOUNTS}

.PHONY: vend
vend: _validate ## Create the directory structure for a new account. Requires $ACCOUNT_PATH e.g org/cop/dev
	scripts/vend.sh $(ACCOUNT_PATH)

.PHONY: cleanup
cleanup: ## Clean system
	# sudo echo "y" | docker system prune
	rm -f $(ACCOUNT_PATH)/.credentials.aws || true
	rm -f $(TF_PATH)/$(TF_ARTIFACT) || true

.PHONY: policy
policy: _clean _validate ## Execute cloud custodian policies against an account
	echo "Working with [$(TIER)] [$(ACCOUNT)$(ACCOUNT_ENV)] ... [$(ACCOUNT_PATH)]"
	export ACCOUNT_ID=$(ACCOUNT_ID) && \
	export ACCOUNT="nbs-management-$(ACCOUNT)-$(ACCOUNT_ENV)" && \
	export MAKE_SHELL="no" && \
	. scripts/assume-role.sh && \
	$(CUSTODIAN) run /opt/app/compliance/policy.yml -s /opt/app/tests/output/results --region us-east-1


.PHONY: instances
instances: _clean _validate ## Execute cloud custodian policies against an account
	echo "Working with [$(TIER)] [$(ACCOUNT)$(ACCOUNT_ENV)] ... [$(ACCOUNT_PATH)]"
	export ACCOUNT_ID=$(ACCOUNT_ID) && \
	export ACCOUNT="nbs-management-$(ACCOUNT)-$(ACCOUNT_ENV)" && \
	export MAKE_SHELL="no" && \
	. scripts/assume-role.sh && \
	./scripts/getinstances.sh

##@ Misc
.PHONY: help
help: ## Display this help
	awk \
	  'BEGIN { \
	    FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n" \
	  } /^[a-zA-Z_-]+:.*?##/ { \
	    printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 \
	  } /^##@/ { \
	    printf "\n\033[1m%s\033[0m\n", substr($$0, 5) \
	  } ' $(MAKEFILE_LIST)


##@ Helpers
.PHONY: _init
_init:  _tokenize ## Initialise terraform state file with S3/DynamoDB lock
	$(TF) init -backend-config=key=$(TF_STATE_KEY) -reconfigure >&2

.PHONY: _tokenize
_tokenize: ## Request 2FA code and provide credentials for terraform
	echo "Getting a session token"
	docker-compose run --entrypoint=bash aws scripts/tokenize.sh

.PHONY: _clean
_clean: ## Remove terraform directory and any left over docker networks
	echo "Removing .terraform directory and purging orphaned docker networks"
	docker-compose run --entrypoint="rm -rf .terraform/modules" terraform
	docker-compose down --remove-orphans 2>/dev/null

.PHONY: _validate
_validate: ## Validate the environment variables are in the accepted list
	[[ "$(ACCOUNT_PATH)" =~ $(RE_ACCOUNT_PATH) ]] || (echo "$(ACCOUNT_PATH) is not a valid option for ACCOUNT_PATH var, see README" && exit 1)
	[[ "$(TIER)" =~ $(RE_TIER) ]] || (echo "$(TIER) is not a valid option for TIER, see README" && exit 1)
