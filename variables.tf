# Variables
variable "region" {
    default = "westeurope"
}

variable "resource_group" {
    default = "devopsgitlab"
}

variable "virtual_network_name" {
    default = "GitlabVNET"
}

variable "username" {
    default = "devops"
}

variable "ssh_key_location" {
    default = "~/.ssh/gitlab.pub"
}

variable "gitlab_vm_size" {
    default = "Standard_A1_v2"
}

variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
